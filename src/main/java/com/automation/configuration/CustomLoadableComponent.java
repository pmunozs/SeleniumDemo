package com.automation.configuration;

public abstract class CustomLoadableComponent <T extends CustomLoadableComponent<T>> {

	@SuppressWarnings("unchecked")
	public T get() {
		
		try {
			
			load();
			return (T) this;
			
		} catch (Error e) {
			
			System.out.println("Error while loading page: " + e.getMessage());
			
		} finally {
			
			isLoaded();
		}
		
		return (T) this;
	}
	
	protected abstract void load();
	
	protected abstract void isLoaded() throws Error;
}
