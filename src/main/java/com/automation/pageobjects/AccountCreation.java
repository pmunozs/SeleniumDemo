package com.automation.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class AccountCreation extends BasePage<AccountCreation>{

	private WebDriver driver;
	private String baseURL = "http://automationpractice.com/index.php?controller=authentication&back=my-account#account-creation";
	private String pageTitle = "Login - My Store";
	
	@FindBy(id = "customer_firstname")
	WebElement personalFirstName;
	
	@FindBy(id = "customer_lastname")
	WebElement personalLastName;
	
	@FindBy(id = "email")
	WebElement personalEmail;
	
	@FindBy(id = "passwd")
	WebElement personalPassword;
	
	@FindBy(id = "days")
	WebElement personalDay;
	
	@FindBy(id = "months")
	WebElement personalMonth;
	
	@FindBy(id = "years")
	WebElement personalYear;
	
	@FindBy(id = "newsletter")
	WebElement personalNewsletter;
	
	@FindBy(id = "optin")
	WebElement personalOffer;
	
	@FindBy(id = "firstname")
	WebElement addressFirstname;
	
	@FindBy(id = "lastname")
	WebElement addressLastname;
	
	@FindBy(id = "company")
	WebElement addressCompany;
	
	@FindBy(id = "address1")
	WebElement addressOne;
	
	@FindBy(id = "address2")
	WebElement addressTwo;
	
	@FindBy(id = "city")
	WebElement addressCity;
	
	@FindBy(id = "id_state")
	WebElement addressState;
	
	@FindBy(id = "postcode")
	WebElement addressPostcode;
	
	@FindBy(id = "id_country")
	WebElement addressCountry;
	
	@FindBy(id = "other")
	WebElement addressInformation;
	
	@FindBy(id = "phone")
	WebElement addressHomePhone;
	
	@FindBy(id = "phone_mobile")
	WebElement addressMobilePhone;
	
	@FindBy(id = "alias")
	WebElement addressAlias;
	
	@FindBy(id = "submitAccount")
	WebElement Register;
	
	public AccountCreation(WebDriver driver) {
		
		super(driver);
		this.driver = driver;
	}

	public void setPersonalInformation(String firstName, String lastName, String email,
			String password, String dayOfBirth, String monthOfBirth, String yearOfBirth,
			boolean newsletter, boolean offer ) {
		
		waitForElementToBeClickable(personalFirstName);
		
		personalFirstName.sendKeys(firstName);
		personalLastName.sendKeys(lastName);
		
		if(!email.isEmpty()) {
			
			personalEmail.clear();
			personalEmail.sendKeys(email);
		}
		
		personalPassword.sendKeys(password);
		
		Select slcDay = new Select(personalDay);
		slcDay.selectByValue(dayOfBirth);
		
		Select slcMonth = new Select(personalMonth);
		slcMonth.selectByValue(monthOfBirth);
		
		Select slcYear = new Select(personalYear);
		slcYear.selectByValue(yearOfBirth);
		
		if(newsletter) {
			
			personalNewsletter.click();
		}
		
		if(offer) {
			
			personalOffer.click();
		}
	}
	
	public void setAddressInformation(String firstName, String lastName, String company,
			String address, String addres2, String city, String state, String zipCode,
			String country, String additionalInformation, String homePhone, String mobilePhone, String alias) {
		
		if(!firstName.isEmpty()) {
			
			addressFirstname.clear();
			addressFirstname.sendKeys(firstName);
		}
		
		if(!lastName.isEmpty()) {
			
			addressLastname.clear();
			addressLastname.sendKeys(lastName);
		}
		
		addressCompany.sendKeys(company);
		addressOne.sendKeys(address);
		addressTwo.sendKeys(addres2);
		addressCity.sendKeys(city);
		
		Select slcState = new Select(addressState);
		slcState.selectByVisibleText(state);
		
		addressPostcode.sendKeys(zipCode);
		
		Select slcCountry = new Select(addressCountry);
		slcCountry.selectByVisibleText(country);		
		
		addressInformation.sendKeys(additionalInformation);
		
		addressHomePhone.sendKeys(homePhone);
		addressMobilePhone.sendKeys(mobilePhone);
		
		if(!alias.isEmpty()) {
			
			addressAlias.clear();
			addressAlias.sendKeys(alias);
		}		
	}
	
	@Override
	protected void load() {
		
		driver.get(baseURL);
	}

	@Override
	protected void isLoaded() throws Error {
		
		Assert.assertEquals(driver.getCurrentUrl(), baseURL);
		Assert.assertEquals(driver.getTitle(), pageTitle);
	}
}
