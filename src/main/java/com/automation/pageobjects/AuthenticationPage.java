package com.automation.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class AuthenticationPage extends BasePage<AuthenticationPage> {

	private WebDriver driver;
	private String baseURL = "http://automationpractice.com/index.php?controller=authentication&back=my-account";
	private String pageTitle = "Login - My Store";
	
	@FindBy(id = "email")
	WebElement loginEmail;
	
	@FindBy(id = "passwd")
	WebElement loginPassword;
	
	@FindBy(id = "SubmitLogin")
	WebElement loginSubmit;
	
	@FindBy(id = "email_create")
	WebElement registerEmail;
	
	@FindBy(id = "SubmitCreate")
	WebElement registerSubmit;
	
	public AuthenticationPage(WebDriver driver) {
		
		super(driver);
		this.driver = driver;
	}

	public MyAccount login(String email, String password) {
		
		loginEmail.sendKeys(email);
		loginPassword.sendKeys(password);
		loginSubmit.click();
		
		return new MyAccount(driver);
	}
	
	public AccountCreation register(String email) {
		
		registerEmail.sendKeys(email);
		registerSubmit.click();
		
		return new AccountCreation(driver);
	}
	
	@Override
	protected void load() {
		
		driver.get(baseURL);
	}

	@Override
	protected void isLoaded() throws Error {
		
		Assert.assertEquals(driver.getCurrentUrl(), baseURL);
		Assert.assertEquals(driver.getCurrentUrl(), pageTitle);
	}
}
