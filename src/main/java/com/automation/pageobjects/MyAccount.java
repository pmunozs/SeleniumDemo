package com.automation.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class MyAccount extends BasePage<MyAccount>{

	private WebDriver driver;
	private String baseURL = "http://automationpractice.com/index.php?controller=my-account";
	private String pageTitle = "My account - My Store";
	
	@FindBy(xpath = "//a[@title = 'Orders']")
	WebElement OrderHistory;
	
	@FindBy(xpath = "//a[@title = 'Credit slips']")
	WebElement CreditSlips;
	
	@FindBy(xpath = "//a[@title = 'Information']")
	WebElement PersonalInformation;
	
	@FindBy(xpath = "//a[@title = 'My wishlists']")
	WebElement MyWishlist;
	
	public MyAccount(WebDriver driver) {
		
		super(driver);
		this.driver = driver;
	}

	@Override
	protected void load() {
		
		driver.get(baseURL);		
	}

	@Override
	protected void isLoaded() throws Error {
		
		Assert.assertEquals(driver.getCurrentUrl(), baseURL);
		Assert.assertEquals(driver.getTitle(), pageTitle);
	}
}
