
package com.automation.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.automation.configuration.CustomLoadableComponent;

public abstract class BasePage <T extends CustomLoadableComponent<T>> extends CustomLoadableComponent<T> {
	
	private WebDriver driver;
	
	@FindBy(linkText = "Sign in")
	WebElement SignIn;
	
	@FindBy(linkText = "Contact us")
	WebElement ContactUs;
	
	@FindBy(className = "account")
	WebElement SignedAccount;
	
	@FindBy(className = "logout")
	WebElement Logout;
	
	public BasePage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getSignedAccount() {
		
		return SignedAccount;
	}
	
	public WebElement getLogout() {
		
		return Logout;
	}
	
	public void clickOn(WebElement component) {
		
		component.click();
	}
	
	public void clickOnByText(String componentText) {
		
		WebElement component = driver.findElement(By.xpath("//*[text() = '" + componentText + "']"));
		clickOn(component);
	}
	
	public AuthenticationPage clickSignIn() {
		
		if(elementIsDisplayed(SignedAccount)) {
			
			clickOn(Logout);
		}
		
		clickOn(SignIn);
		return new AuthenticationPage(driver);
	}
	
	public void waitForElementToBeClickable(WebElement component) {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(component));	
	}
	
	public boolean elementIsDisplayed(WebElement component) {
		
		boolean displayed = true;
		
		try {
			
			Assert.assertTrue(component.isDisplayed());
			
		} catch (Exception e) {
			
			displayed = false;
		}
		
		return displayed;
	}
}
