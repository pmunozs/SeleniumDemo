package com.automation.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class ContactUs extends BasePage<ContactUs>{

	private WebDriver driver;
	private String baseURL = "http://automationpractice.com/index.php?controller=contact";
	private String pageTitle = "Contact us - My Store";
	
	public enum AlertMessage {
		
		SUCCESS_MESSAGE("Your message has been successfully sent to our team."),
	    EMAIL_ERROR_MESSAGE ("There is 1 error\nInvalid email address."),
	    INFORMATION_ERROR_MESSAGE ("There is 1 error\nThe message cannot be blank."),
	    SUBJECT_ERROR_MESSAGE("There is 1 error\nPlease select a subject from the list provided.");
    
		private String message;
		
	    AlertMessage (String message) {
			
			this.message = message;
		}
		
		public String getMessage() {
			
			return message;
		}
	}
	
	@FindBy(id = "id_contact")
	WebElement SubjectHeading;
	
	@FindBy(id = "email")
	WebElement EmailAddress;
	
	@FindBy(id = "id_order")
	WebElement OrderReference;
	
	@FindBy(id = "fileUpload")
	WebElement AttachFile;
	
	@FindBy(id = "message")
	WebElement Message;
	
	@FindBy(id = "submitMessage")
	WebElement Send;
	
	@FindBy(className = "alert")
	WebElement Alert;
	
	@FindBy(id = "desc_contact1")
	WebElement HelperWebMaster;
	
	@FindBy(id = "desc_contact2")
	WebElement HelperCustomer;
	
	public WebElement alert() {
		
		return this.Alert;
	}
	
	public ContactUs(WebDriver driver) {
		
		super(driver);
		this.driver = driver;
	}
	
	public void fillMessage(String subjectHeading, String email, String order, String file, String message) {
		
		Select slcSubjectHeading = new Select(SubjectHeading);
		slcSubjectHeading.selectByVisibleText(subjectHeading);
		EmailAddress.sendKeys(email);
		OrderReference.sendKeys(order);
		AttachFile.sendKeys(file);
		Message.sendKeys(message);
	}
	
	public void sendMessage() {
		
		clickOn(Send);
	}

	@Override
	protected void load() {
		
		driver.get(baseURL);
	}

	@Override
	protected void isLoaded() throws Error {
		
		Assert.assertEquals(driver.getCurrentUrl(), baseURL);
		Assert.assertEquals(driver.getTitle(), pageTitle);
	}
}
