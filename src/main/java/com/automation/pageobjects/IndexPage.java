package com.automation.pageobjects;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class IndexPage extends BasePage<IndexPage> {

	private WebDriver driver;
	private String baseURL = "http://automationpractice.com/index.php";
	private String pageTitle = "My Store";
	
	public IndexPage(WebDriver driver) {
		
		super(driver);
		this.driver = driver;
	}

	@Override
	protected void load() {
		
		driver.get(baseURL);
	}

	@Override
	protected void isLoaded() {
		
		Assert.assertEquals(driver.getTitle(), pageTitle);
		Assert.assertEquals(driver.getCurrentUrl(), baseURL);	
	}

}
