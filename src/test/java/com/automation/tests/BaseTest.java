package com.automation.tests;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;

public class BaseTest {

	 WebDriver driver;
	
  @BeforeClass
  public void beforeClass() {
	  
	System.setProperty("webdriver.chrome.driver", "selenium/chromedriver.exe");
	ChromeOptions options = new ChromeOptions();
	options.addArguments("start-maximized");
	options.addArguments("disable-infobars");
	driver = new ChromeDriver(options);
  }

  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

}
