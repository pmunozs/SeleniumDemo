package com.automation.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automation.pageobjects.AccountCreation;
import com.automation.pageobjects.AuthenticationPage;
import com.automation.pageobjects.IndexPage;
import com.automation.pageobjects.MyAccount;

public class IndexTest extends BaseTest {
	
	IndexPage index;
	 
	@BeforeMethod
	public void Initialize() {
		
		index = new IndexPage(driver);
		index.get();
	}
	
	@Test(description = "Verifies login flow is working as expected")
	public void verifyLogin() {
			
		AuthenticationPage authentication = index.clickSignIn();
		MyAccount myAccount = authentication.login("john.doe@mail.com", "pass1234");
		
		Assert.assertTrue(myAccount.getLogout().isDisplayed());
		Assert.assertEquals(myAccount.getSignedAccount().getText(), "John Doe");	
	}
	
	@Test(description = "Verifies register flow is working as expected")
	public void verifyRegistration() {
		
		AuthenticationPage authentication = index.clickSignIn();
		AccountCreation accountCreation = authentication.register("unknown.user@mail.com");
		accountCreation.setPersonalInformation("Pablo", "Mu�oz", "pablo.munoz.sommaruga@gmail.com", "hola1234", "23", "10", "1990", true, true);
		accountCreation.setAddressInformation("Pablo", "Mu�oz", "Conexio Group", "Calle 1234", "Calle 987", "Montevideo", "Rhode Island" ,
				"1234", "United States", "This is additional information", "+(598) 123 123", "+(560) 123 123", "This is my alias");
	}
}
