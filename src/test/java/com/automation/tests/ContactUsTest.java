package com.automation.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.automation.pageobjects.ContactUs;
import com.automation.pageobjects.ContactUs.AlertMessage;

public class ContactUsTest extends BaseTest {
	
	ContactUs contactPage;
	String currentPath = System.getProperty("user.dir");
	
	@BeforeMethod
	public void InitializePage() {
		
		contactPage = new ContactUs(driver);
		contactPage.get();	
	}
	
	@DataProvider(name = "dataProviderSuccessMessage")
	public Iterator<Object []> provider( ) throws Exception {
		
		List<Object[]> testCases = new ArrayList<Object[]>();
		String[] data = null;
		BufferedReader br = null;
		File csvFile = new File(currentPath + "/selenium/data-files/ContactUs - Data.csv");
		String line = "";
		String cvsSpliBy = ",";
		
		try {
			
			br = new BufferedReader(new FileReader(csvFile));
			
			while((line = br.readLine()) != null) {
				
				data = line.split(cvsSpliBy);
				testCases.add(data);
			}
			
		} catch (FileNotFoundException e) {
			
			System.out.println("Error message: " + e.getMessage());
		}	
		
		return testCases.iterator();	
	}
	
    @Test(dataProvider = "dataProviderSuccessMessage")
    public void sendMessageSuccessfully (String subject, String email, String order, String image, String message) {
	 
	   contactPage.fillMessage(subject, email, order, currentPath + image, message);   
	   contactPage.sendMessage();    
	   
	   assert contactPage.alert().isDisplayed() : "Alert message not visible. Please verify";
	   assert contactPage.alert().getText().equals(AlertMessage.SUCCESS_MESSAGE.getMessage()) : "Success message doesn't match"; 
    }
    
    @Test
    public void showsInvalidEmailMessage() {
    	
    	contactPage.fillMessage("Customer service", "mail", "12341234", currentPath + "/selenium/test-img.png", "");
    	contactPage.sendMessage();
	
    	assert contactPage.alert().isDisplayed() : "Alert message not visible. Please verify";
    	assert contactPage.alert().getText().equals(AlertMessage.EMAIL_ERROR_MESSAGE.getMessage()) : "Error message doesn't match";
    }
    
    @Test
    public void showsInvalidOptionMessage() {
    	
		contactPage.fillMessage("-- Choose --", "mail@mail.com", "12341234", currentPath + "/selenium/test-img.png", "Non empty message");
		contactPage.sendMessage();
		
		assert contactPage.alert().isDisplayed() : "Alert message not visible. Please verify";
		assert contactPage.alert().getText().equals(AlertMessage.SUBJECT_ERROR_MESSAGE.getMessage()) : "Error message doesn't match";	 	
    }
    
    @Test
    public void showsBlankMessage() {
    	
    	contactPage.fillMessage("-- Choose --", "mail@mail.com", "12341234", currentPath + "/selenium/test-img.png", "");
    	contactPage.sendMessage();
    	
    	assert contactPage.alert().isDisplayed() : "Alert message not visible. Please verify";
		assert contactPage.alert().getText().equals(AlertMessage.INFORMATION_ERROR_MESSAGE.getMessage()) : "Error message doesn't match";				
    }
}
