# Selenium Demo

Test Automation demo

## Built With
* Selenium (http://www.seleniumhq.org/)
* Java (https://www.java.com/en/download/)
* TestNG (http://testng.org/doc/index.html)
* Maven (https://maven.apache.org/)
* Eclipse (https://www.eclipse.org/)

## Author
* Pablo Muñoz

## License
* https://creativecommons.org/licenses/by/4.0/

## Acknowledgments
* http://toolsqa.com/
* https://github.com/sargissargsyan/page-objects-loadable-component
* http://automationpractice.com/index.php
* https://gist.github.com/PurpleBooth/109311bb0361f32d87a2